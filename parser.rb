require_relative 'lib/log_counter'

parser = LogCounter.new(path: ARGV[0], unique_visits: ARGV[1] || false)
parser.run
