class LogCounter
  attr_reader :file_path, :unique_visits, :page_visits

  def initialize(path:, unique_visits: false)
    validate_path(path)

    @file_path = path
    @unique_visits = unique_visits
    @page_visits = {}
  end

  def run
    parse
    visits = sum_visits
    sorted = sort_descending(visits)
    print sorted
    sorted
  end

  private

  def validate_path(path)
    raise "invalid file path: #{path}" unless File.exists? path
  end

  def parse
    File.open(file_path) do |file|
      file.each_line do |line|
        url, visitor = line.split(' ')
        page_visits[url] ||= []
        page_visits[url] << visitor if visitor
      end
    end
    page_visits
  end

  def sum_visits
    page_visits.map do |url, visitors|
      visitors = unique_visits ? visitors.uniq : visitors
      visitor_count = visitors.count
      [url, visitor_count]
    end.to_h
  end

  def sort_descending(visits)
    visits.sort_by { |_url, visit_count| visit_count }.reverse.to_h
  end
end
