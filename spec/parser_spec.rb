require 'bundler/inline'
require_relative '../lib/log_counter.rb'

gemfile do
  source 'https://rubygems.org'
  gem 'rspec'
end

RSpec.describe LogCounter do
  let(:path) { 'spec/fixtures/sample.log' }
  let(:unique_visits) { false }

  subject(:log_counter) { described_class.new(path: path, unique_visits: unique_visits) }

  describe '.intialize' do
    context 'when file path is invalid' do
      let(:path) { 'invalid_path' }

      it 'raises an error' do
        expect { log_counter }.to raise_error("invalid file path: #{path}")
      end
    end

    it 'returns an instance of LogCounter' do
      expect(log_counter).to be_a(LogCounter)
    end
  end

  describe '#run' do
    let(:expected_results) do
      {
        '/about/2' => 3,
        '/home' => 2,
        '/help_page/' => 2,
        '/about' => 1,
        '/contact' => 1,
        '/new/' => 0
      }
    end

    let(:parsed) do
      {
        '/about' => ['897.280.786.156'],
        '/about/2' => ['444.701.448.104', '836.973.694.403', '444.701.448.104'],
        '/contact' => ['184.123.665.067'],
        '/help_page/' => ['444.701.448.104', '444.701.448.104'],
        '/home' => ['126.318.035.038', '715.156.286.412'],
        '/new/' => []
      }
    end

    subject(:results) { log_counter.run }

    it 'parses the file' do
      log_counter.run
      expect(log_counter.page_visits).to eq parsed
    end

    it 'returns the total number of visits for each url' do
      results.each do |url, visit_count|
        expect(expected_results[url]).to eq visit_count
      end
    end

    it 'returns the urls in descending order with most visited first' do
      sorted_visits = results.values
      expect(sorted_visits.sort.reverse).to eq sorted_visits
    end

    context 'with unique_visits true' do
      let(:expected_results) do
        {
          '/about/2' => 2,
          '/home' => 2,
          '/help_page/' => 1,
          '/about' => 1,
          '/contact' => 1,
          '/new/' => 0
        }
      end
      let(:unique_visits) { true }

      it 'returns the total number of visits for each url' do
        results.each do |url, visit_count|
          expect(expected_results[url]).to eq visit_count
        end
      end

      it 'returns the urls in descending order with most visited first' do
        sorted_visits = results.values
        expect(sorted_visits.sort.reverse).to eq sorted_visits
      end
    end
  end
end
