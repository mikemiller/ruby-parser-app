# Parser

The parser takes two named arguments, :file_path and :unique_visits. :file_path is required and :unique_visits is optional and will default to false when not given.

When :unique_visits is true, the parser will not count repeat visits from the same address.

## To use this script from within the directory:

## For unique visits run:

`ruby parser.rb webserver.log true`

## For all visits run:

`ruby parser.rb webserver.log`

## For specs run:

`rspec`

## Notes

I used TDD and began by writing the tests for the LogCounter.

I went for a simple approach with just two public methods for the LogCounter, :initialize and :run.

By adding the :unique_visits argument to the :initialize method, I was able to DRY the code considerably.

The benefit of making much of the class private and having limited public methods means that if we were to refactor the logic in this class, there would be very little to update on any services that call it. It's much more reusable as a result and I was still able to test each requirement was met.

I also tested edge cases that include:

- urls that have no visitors in the logfile
- when the logfile does not exist.

Once I'd met the requirements, I created a script that would enable the user to call the class directly from the command line.

If I were to extend this I'd consider the following:

- checking if the log_counter has already run, if so, return the same values rather than calculating them again.
- validating the values in the logfile
- better formatting of the output
- a dashboard to make the information more accessible for teammates/stakeholders.
- refactoring the tests to use shared_examples.
